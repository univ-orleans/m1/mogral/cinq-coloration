# Cinq Coloration



**date:** 20 octobre au 13 décembre 2020<br/>
**sujet:** [projet.pdf](projet.pdf)<br/>
**langage:** Python<br/>
**version minimum:** 3.8<br/>
<br/><br/>


## Objectif
Faire une application du problème de coloration avec un ensemble au plus de 5 couleurs. À partir un graphe donné, réaliser une coloration de chaque sommet de sorte à ce les sommets adjacents n'aient pas la même couleur.

<br/>
<img src="out/JoliGraphe12.svg" alt="Image SVG du rendu du graphe planaire connexe à 12 sommets." height="400" style="margin:0 auto; width:100%;"/>

<br/><br/>

## Commandes d'exécution
**Consulter la documentation courte**
```bash
$ cd cinq-coloration/src/
$ python3 -m coloration -h
```
<br/>

**Trouver une coloration**
```bash
$ cd cinq-coloration/src/
$ python3 -m coloration ../data/JoliGraphe10.graphe
```
>Entrée: `data/JoliGraphe10.graphe`.<br/>
Sortie: `out/JoliGraphe10.colors`.

<br/>

**Générer le SVG de la coloration du graphe**
```bash
$ cd cinq-coloration/src/
$ python3 -m coloration ../data/JoliGraphe10.graphe --coords ../data/JoliGraphe10.coords
```
>Entrée: `data/JoliGraphe10.graphe` et `data/JoliGraphe10.coords`.<br/>
Sortie: `out/JoliGraphe10.colors` et `out/JoliGraphe10.svg`.

<br/>

**Mode débogage**

Affiche l'état du graphe avant et après coloration.
```bash
$ cd cinq-coloration/src/
$ python3 -m coloration ../data/JoliGraphe10.graphe --debug
```
>Entrée: `data/JoliGraphe10.graphe`.<br/>
Sortie: `out/JoliGraphe10.colors`.

<br/><br/>

**Mode log**

Affiche les logs d'exécution du programme.
```bash
$ cd cinq-coloration/src/
$ python3 -m coloration ../data/JoliGraphe10.graphe --log
```
>Entrée: `data/JoliGraphe10.graphe`.<br/>
Sortie: `out/JoliGraphe10.colors`.

<br/><br/>


## Contributeurs
- Arnaud ORLAY (@arnorlay) : Master 1 IMIS Informatique
- Marion JURÉ (@marionjure) : Master 1 IMIS Informatique
