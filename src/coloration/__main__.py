# ==================== INFORMATIONS
__version__ = "1.0.0"
__author__ = "Arnaud Orlay <arnaud.orlay@etu.univ-orleans.fr>"



# ==================== EXPORT
__all__ = ["args"]



# ==================== IMPORT
import os
import argparse
import timeit
from datetime import datetime
from coloration.utils import file
from coloration.utils import svg



# ==================== FUNCTION
def formatTimePercent(time: float, total_time: float, decimal_round: int) -> str:
	percent = round(time * 100 / total_time, decimal_round)
	nb_decimal = len(str(percent)[str(percent).index(".")+1:])
	is_float = 1
	if not decimal_round:
		is_float = 0
		percent = str(int(percent))
	elif nb_decimal != decimal_round:
		percent = str(percent) + "0" * (decimal_round - nb_decimal)
	else:
		percent = str(percent)
	
	return f"\033[90m[{' ' * ((3 + decimal_round + is_float) - len(percent))}{percent}%]\033[0m"


def formatTime(time: float, decimal_round: int, size: int=7):
	time = f"{round(time, decimal_round)}s"
	return f"{time}{' ' * (size - len(time))}"




# ==================== RUN 
# Récupération des paramètres passées en argument
parser = argparse.ArgumentParser(prog="coloration", description="Script de coloration d'un graphe en 5 couleurs maximum.")
parser.add_argument("graph_file", nargs=1, type=str, help="Graph file.")
parser.add_argument("--coords", nargs=1, type=str, help="Coordinate file.")
parser.add_argument("-d", "--debug", action="store_true", help="Print data structure in user terminal.")
parser.add_argument("--log", action="store_true", help="Print log in user terminal.")
args = parser.parse_args()



if __name__ == "__main__":

	time_run_start = datetime.now()

	# Création du graphe avec ou sans coordonnées
	time_file_graphe_start = datetime.now()
	graph = file.readFileGraph(filepath=args.graph_file[0])
	time_file_graphe = (datetime.now() - time_file_graphe_start).total_seconds()
	if args.coords != None:
		time_file_coords_start = datetime.now()
		graph = file.readFileCoords(filepath=args.coords[0], graph=graph)
		time_file_coords = (datetime.now() - time_file_coords_start).total_seconds()

	# affichage des données du graphe avant coloration
	if args.debug: print("\nAvant coloration:", graph)

	# Coloration
	time_coloring_start = datetime.now()
	graph.coloring()
	time_coloring = (datetime.now() - time_coloring_start).total_seconds()

	# affichage des données du graphe avant coloration
	if args.debug: print("\nAprès coloration:", graph)

	# Vérification de la coloration
	time_cheking_start = datetime.now()
	graph.checkColoring()
	time_cheking = (datetime.now() - time_cheking_start).total_seconds()

	# Récupération de la racine du projet
	work_directory = "/".join(os.getcwd().split("/")[:-1])

	# récupération du nom de fichier
	file_name = args.graph_file[0].split("/")[-1][:-7]

	# génération du fichier de couleur
	time_file_colors_start = datetime.now()
	file.writeFileColor(f"{work_directory}/out/{file_name}.colors", graph)
	time_file_colors = (datetime.now() - time_file_colors_start).total_seconds()

	# génération du graphe dans un fichier image au format svg
	if graph.hasCoords:
		time_file_svg_start = datetime.now()
		svg.saveSvgFile(f"{work_directory}/out/{file_name}.svg", graph)
		time_file_svg = (datetime.now() - time_file_svg_start).total_seconds()


	time_run = (datetime.now() - time_run_start).total_seconds()



	# affichage de la fiche d'exécution du programme
	line_size = 50
	time_round = 4
	percent_round = 2
	time_other = time_run - sum([time_coloring, time_cheking, time_file_graphe, time_file_colors])
	if graph.hasCoords: time_other -= sum([time_file_coords, time_file_svg])
	line = "~" * line_size
	has_coords = " "
	if graph.hasCoords: has_coords = "x"
	
	print(line)
	print("\033[1m5 COLORATION\033[0m")
	
	print("\n\033[1mGraphe\033[0m")
	print(f"nom:         {file_name}")
	print(f"sommets:     {graph.size}")
	
	print("\n\033[1mTemps d'exécutions\033[0m")
	print(f"fichier graphe:  {formatTime(time_file_graphe, time_round)}  {formatTimePercent(time=time_file_graphe, total_time=time_run, decimal_round=percent_round)}")
	if graph.hasCoords: print(f"fichier coords:  {formatTime(time_file_coords, time_round)}  {formatTimePercent(time=time_file_coords, total_time=time_run, decimal_round=percent_round)}")
	print(f"fichier colors:  {formatTime(time_file_colors, time_round)}  {formatTimePercent(time=time_file_colors, total_time=time_run, decimal_round=percent_round)}")
	if graph.hasCoords: print(f"fichier svg:     {formatTime(time_file_svg, time_round)}  {formatTimePercent(time=time_file_svg, total_time=time_run, decimal_round=percent_round)}")
	print(f"coloration:      {formatTime(time_coloring, time_round)}  {formatTimePercent(time=time_coloring, total_time=time_run, decimal_round=percent_round)}")
	print(f"vérification:    {formatTime(time_cheking, time_round)}  {formatTimePercent(time=time_cheking, total_time=time_run, decimal_round=percent_round)}")
	print(f"autre:           {formatTime(time_other, time_round)}  {formatTimePercent(time=time_other, total_time=time_run, decimal_round=percent_round)}")
	print(f"total:           {formatTime(time_run, time_round)}  {formatTimePercent(time=time_run, total_time=time_run, decimal_round=percent_round)}")

	print("\n\033[1mFichiers d'entrée\033[0m")
	print(f"graphe: [x]")
	print(f"coords: [{has_coords}]")

	print("\n\033[1mFichiers de sortie\033[0m")
	print(f"colors: [x]")
	print(f"svg:    [{has_coords}]")
	print(line)
