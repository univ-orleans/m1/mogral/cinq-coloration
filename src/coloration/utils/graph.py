# ==================== INFORMATIONS
__version__ = "1.0.0"
__author__ = "Arnaud Orlay <arnaud.orlay@etu.univ-orleans.fr>"



# ==================== EXPORT
__all__ = ["Graph"]



# ==================== IMPORT
from coloration.utils import log
import copy
from random import randint



# ==================== CLASS
class Graph():
	NEIGHBORS = "neighbors"
	COORDINATE = "coordinate"
	COLOR = "color"
	ACTIVE = "active"
	COLOR_ANY = "*"
	COLOR_RED = "red"
	COLOR_BLUE = "blue"
	COLOR_BLACK = "black"
	COLOR_WHITE = "white"
	COLOR_GREEN = "green"
	LIST_COLOR = [COLOR_RED,COLOR_BLUE,COLOR_BLACK,COLOR_WHITE,COLOR_GREEN]
	
	
	
	def __init__(self, size: int=1):
		"""
			Build a graph.

			Instantiates the nodes of the graph from the number provided in 
			parameter. Each node has an empty adjacency list at initialization.

			:param  size:   the number of node (default 0).
			:type   size:   int
			:return:        new graph
			:rtype:         Graph
		"""
		self.__size = size
		self.__size_init = size
		self.__nodes = dict()
		self.__has_coords = False
		
		for i in range(self.__size):
			self.__nodes[str(i)] = {self.COLOR: self.COLOR_ANY, self.COORDINATE: "*", self.NEIGHBORS: list() , self.ACTIVE : True}
		
		log.info(f"Création du graphe à {size} noeuds.")
	
	
	
	def __repr__(self):
		repr = str("\n")
		length = len(str(self.size))
		title_node = "Nodes"
		title_color = "Color"
		title_coords = "Coords  "
		title_neighbors = "Neighbors"
		repr += f"{title_node}  {title_color}  {title_coords}  {title_neighbors}\n"
		repr += f"{'-'*len(title_node)}  {'-'*len(title_color)}  {'-'*len(title_coords)}  {'-'*len(title_neighbors)*10}\n"
		for i in range(self.size):
			node = str(i)
			data = self.__nodes[node]
			repr += f"{node}{' '*(len(title_node)-len(node))}  {data[self.COLOR]}{' '*(len(title_color)-len(data[self.COLOR]))}  {data[self.COORDINATE]}{' '*(len(title_coords)-len(str(data[self.COORDINATE])))}  {data[self.NEIGHBORS]}\n"

		return repr
	
	
	
	@property
	def size(self) -> int:
		"""Return the number of node"""
		return self.__size
	


	@property
	def sizeInit(self) -> int:
		"""Return the number of node"""
		return self.__size_init



	@property
	def hasCoords(self) -> bool:
		"""Return if data structure has set coordinates"""
		return self.__has_coords
	
	
	
	def decreaseSize(self) -> None:
		"""Decrease the number of node"""
		self.__size -=1
	
	
	
	def increaseSize(self) -> None:
		"""Increase the number of node"""
		self.__size +=1
	
	
	
	def enableCoords(self) -> None:
		"""Enable attribute has_coords to True"""
		self.__has_coords = True
	
	
	
	def getNode(self , node: int) -> None :
		"""
			Get the node.

			:param node:	the node.
			:type  node:    int
			:return:        the node
			:rtype:         dict

			:Exemple:
			>>> graph = graph.Graph(3)
			>>> graph.getNode(0)
			{'color': '*', 'coordinate': (0, 0), 'neighbors': []}
		"""
		return self.__nodes[str(node)];
	
	
	
	def getNodes(self) -> dict:
		"""
			Get the node.

			:return:	thes nodes
			:rtype:		dict

			:Exemple:
			>>> graph = graph.Graph(3)
			>>> graph.getNodes()
			{"0":{'color': '*', 'coordinate': (0, 0), 'neighbors': []}, "1": ... , "2": ...}
		"""
		return self.__nodes
	
	
	
	def getNeighbors(self, node: int) -> list:
		"""
			Get adjacency list of node.

			:param node:    the node.
			:type  node:    int
			:return:        adjacency list composed of int.
			:rtype:         list

			:Exemple:
			>>> graph = graph.Graph(3)
			>>> graph.getNeighbors(0)
			[]

			.. warning::    Node index can't be less than 0 or more than attribute `size`.
		"""
		if (0 <= node < self.sizeInit):
			list_neighbors = list()
			for n in self.__nodes[str(node)][self.NEIGHBORS]: 
				if self.getActive(n):
					list_neighbors.append(n)
			return list_neighbors
		else:
			log.error(f"La valeur de l'argument 'node' passé à '{node}' ne peut pas être inférieur à '0' ou supérieur ou égale à '{self.sizeInit}'")
	
	
	
	def getNumberOfNeighbors(self, node: int) -> int:
		"""
			Get size of adjacency list of node.

			:param node:    the node.
			:type  node:    int
			:return:        size of adjacency list.
			:rtype:         int

			:Exemple:
			>>> graph = graph.Graph(3)
			>>> graph.getNumberOfNeighbors(0)
			0

			.. warning::    Node index can't be less than 0 or more than attribute `size`.
		"""
		return len(self.getNeighbors(node))
	
	
	
	def addNeighbor(self, node: int, neighbor: int) -> None:
		"""
			Add a node to adjacency list.

			:param node:        the node.
			:param neighbor:    new adjacency node.
			:type  node:        int
			:type  neighbor:    int
			:return:            none.
			:rtype:             None

			:Exemple:
			>>> graph = graph.Graph(3)
			>>> graph.getNeighbors(0)
			[]
			>>> graph.addNeighbor(0, 1)
			>>> graph.getNeighbors(0)
			[1]

			.. warning::    Node index can't be less than 0 or more than attribute `size`.
		"""
		if (0 <= node < self.size):
			if (0 <= neighbor < self.size):
				self.__nodes[str(node)][self.NEIGHBORS].append(neighbor)
			else:
				log.error(f"La valeur de l'argument 'neighbor' passé à '{neighbor}' ne peut pas être inférieur à '0' ou supérieur ou égale à '{self.size}'")
		else:
			log.error(f"La valeur de l'argument 'node' passé à '{node}' ne peut pas être inférieur à '0' ou supérieur ou égale à '{self.size}'")
	
	
	
	def getCoordinate(self, node: int) -> (int, int):
		"""
			Get coordinate of node.

			:param node:    the node.
			:type  node:    int
			:return:        coordinate in plane of degree 2.
			:rtype:         int

			:Exemple:
			>>> graph = graph.Graph(3)
			>>> graph.getCoordinate(0)
			(0, 0)

			.. warning::    Node index can't be less than 0 or more than attribute `size`.
		"""
		return self.__nodes[str(node)][self.COORDINATE]
	
	
	
	def setCoordinate(self, node: int, coordinate: (int, int)) -> None:
		"""
			Set coordinate of node.

			:param node:        the node.
			:param coordinate:  coordinate in plane of degree 2.
			:type  node:        int
			:type  coordinate:  tuple
			:return:            none.
			:rtype:             None

			:Exemple:
			>>> graph = graph.Graph(3)
			>>> graph.setCoordinate(0, (1, 9))

			.. warning::    Node index can't be less than 0 or more than attribute `size`.
		"""
		self.__nodes[str(node)][self.COORDINATE] = coordinate	 
	
	
	
	def getActive(self, node: int) -> bool:
		"""
			Get the state of node. 

			:param node:    the node.
			:type  node:    int
			:return:        True the node is present in the graph.
			:rtype:         bool
			
			:Exemple:
			>>> graph = graph.Graph(3)
			>>> graph.getActive(0)
			True
		"""
		return self.__nodes[str(node)][self.ACTIVE]
	
	
	
	def setActive(self, node: int, active: bool) -> None:
		"""
			Set the state of node.

			:param node:	the node.
			:param active:	the state of the node.
			:type  node:	int
			:type  active:	bool
			:return:		none.
			:rtype:			None
			
			:Exemple:
			>>> graph = graph.Graph(3)
			>>> graph.setActive(0, True)
		"""
		self.__nodes[str(node)][self.ACTIVE] = active
	
	
	
	def getColor(self, node: int) -> str:
		"""
			Get node color.

			:param node:    the node.
			:type  node:    int
			:return:        color of node.
			:rtype:         int

			:Exemple:
			>>> graph = graph.Graph(3)
			>>> graph.getColor(0)
			*

			.. warning::    Node index can't be less than 0 or more than attribute `size`.
			.. note::       At default, color value is '*'. Next node recive color in 'blue', 'red', etc.
		"""
		return self.__nodes[str(node)][self.COLOR]
	
	
	
	def setColor(self, node: int, color: str) -> None:
		"""
			Get node color.

			:param node:    the node.
			:param color:   color.
			:type  node:    int
			:type  color:   int
			:return:        none.
			:rtype:         None

			:Exemple:
			>>> graph = graph.Graph(3)
			>>> graph.setColor(0, graph.COLOR_RED)

			.. warning::    Node index can't be less than 0 or more than attribute `size`.
			.. note::       At default, color value is '*'. Next node recive color in 'blue', 'red', etc.
		"""
		self.__nodes[str(node)][self.COLOR] = color
	
	
	
	def is_empty(self) -> bool:
		"""
			Check if the graph is empty.

			:return:	true if the graph is empty.
			:rtype:		bool

			:Exemple:
			>>> graph = graph.Graph(3)
			>>> graph.is_empty() 
			False

		"""
		return self.size == 0
	
	
	
	def getNodeDegreeInfFive(self) -> int:
		"""
			Get a node of degree less than or equal to 5.

			:return:	a node of degree less than or equal to 5.
			:rtype:		int
			:Exemple:
			>>> graph = graph.Graph(7)
			>>> graph.addNeighbor(6, 1)
			>>> graph.addNeighbor(6, 2)
			>>> graph.addNeighbor(6, 3)
			>>> graph.addNeighbor(6, 4)
			>>> graph.addNeighbor(6, 0)
			>>> graph.addNeighbor(6, 5)
			>>> graph.addNeighbor(1, 0)
			>>> graph.getNodeDegreeInf5()
			1
			.. warning::	There are no nodes of degree less than or equal to 5.
		"""
		for node, value in self.getNodes().items():
			if self.getActive(int(node)):
				if(self.getNumberOfNeighbors(int(node))<=5):
					return int(node)
		log.error(f"Erreur : il n'existe pas de noeuds de degré inférieur ou égale à 5")
	
	
	
	def getColorNotUsedByNeighbors(self, node : int) -> str:
		"""
			Choose a color for a node that is not used by its neighbors.
			
			:param node:	the node.
			:type  node:	int
			:return:		the color chosen for this node.
			:rtype:			str
			:Exemple:
			>>> graph = graph.Graph(3)
			>>> graph.addNeighbor(1, 0)
			>>> graph.addNeighbor(1, 2)
			>>> graph.setColor(0,"red")
			>>> graph.setColor(2,"blue")
			>>> graph.getColorNotUsed(1)
			green
			"""
		color_not_used = list(self.LIST_COLOR)  
		
		for neighbor in self.getNeighbors(node):
			color_neighbor = self.getColor(neighbor)
			if color_neighbor in color_not_used:
				color_not_used.remove(color_neighbor)
				
		if len(color_not_used)==0:
			return self.COLOR_ANY
		val = randint(0, len(color_not_used)-1)
		return color_not_used[val]
	
	
	
	def isLessFiveColor(self, node : int) -> bool:
		"""
			Checks if there are less than 5 colors used by the neighbors of a node.
			
			:param node:        the node.
			:type  node:        int
			:return:            True if there are less than 5 colors used by the neighbors of a node.
			:rtype:             bool
			:Exemple:
			>>> graph = graph.Graph(6)
			>>> graph.addNeighbor(1, 0)
			>>> graph.addNeighbor(1, 2)
			>>> graph.addNeighbor(4, 1)
			>>> graph.addNeighbor(4, 2)
			>>> graph.addNeighbor(4, 3)
			>>> graph.addNeighbor(4, 0)
			>>> graph.addNeighbor(4, 5)
			>>> graph.setColor(1, "red")
			>>> graph.setColor(2, "blue")
			>>> graph.setColor(3, "white")
			>>> graph.setColor(0, "black")
			>>> graph.setColor(5, "green")
			>>> graph.isLessFiveColor(4)
			False
			>>> graph.isLessFiveColor(1)
			True
		"""
		color_not_used = list(self.LIST_COLOR)
		for neighbor in self.getNeighbors(node):
				color_neighbor = self.getColor(neighbor)
				if color_neighbor in color_not_used:
					color_not_used.remove(color_neighbor)
		return len(color_not_used)!=0
	
	
	
	def deleteNode(self ,node: int) -> None:
		"""
			Delete a node from the graphs.
			
			:param node:        the node.
			:type  node:        int
			:return:            none
			:rtype:             None
			:Exemple:
			>>> graph = graph.Graph(3)
			>>> graph.addNeighbor(1, 0)
			>>> graph.addNeighbor(1, 2)
			>>> graph.deleteNode(2)
			{"2":{'color': '*', 'coordinate': (0, 0), 'neighbors': [1],'active':False}, "1": ... }
		"""
		self.setActive(node,False)
		self.decreaseSize()
	
	
	
	def sameRelatedComponent(self , node1 : int , node2: int) ->bool:
		"""
			Checks if two nodes are in the same connected component. (Parcours en profondeur)
			
			:param node1:        the node.
			:type  node1:        int
			:param node2:        the node.
			:type  node2:        int
			:return:            True if two nodes are in the same connected component
			:rtype:             bool
			:Exemple:
			>>> graph.setColor(0 , "white")
			>>> graph.setColor(1 , "*")
			>>> graph.setColor(2 , "black")
			>>> graph.setColor(9 , "black")
			>>> graph.setColor(5 , "red")
			>>> graph.setColor(7 , "red")
			>>> graph.setColor(4 , "blue")
			>>> graph.setColor(10 , "blue")
			>>> graph.setColor(8 , "blue")
			>>> graph.setColor(6 , "green")
			>>> graph.setColor(3 , "green")
			>>> graph.setColor(11 , "green")
			>>> graph.sameRelatedComponent(5, 3)
			True
			>>> graph.sameRelatedComponent(2, 4)
			False
		"""
		visit = {node1}
		file_fifo = [node1]
		while (len(file_fifo)!=0):
			x = file_fifo[0]
			file_fifo.remove(x)
			for neighbor in self.getNeighbors(x): 
				# Vérifie si le noeud est soit de la couleur node1 ou node2 mais il doit etre different de x
				if (self.getColor(neighbor)==self.getColor(node1)  or self.getColor(neighbor)==self.getColor(node2)) and self.getColor(neighbor)!=self.getColor(x):
					if neighbor not in visit:
						file_fifo.append(neighbor)
						visit.add(neighbor)
					if node2 in file_fifo:
						return True	
		return False            
	
	
	
	def swap_color(self , node1 : int , node2: int) -> None:
		"""
			Swap the colors of a connected component. (Parcours en profondeur)
			
			:param node1:        the node.
			:type  node1:        int
			:param node2:        the node.
			:type  node2:        int
			:return:             none
			:rtype:              None
			:Exemple:
			>>> graph.setColor(0 , "white")
			>>> graph.setColor(1 , "*")
			>>> graph.setColor(2 , "black")
			>>> graph.setColor(9 , "black")
			>>> graph.setColor(5 , "red")
			>>> graph.setColor(7 , "red")
			>>> graph.setColor(4 , "blue")
			>>> graph.setColor(10 , "blue")
			>>> graph.setColor(8 , "blue")
			>>> graph.setColor(6 , "green")
			>>> graph.setColor(3 , "green")
			>>> graph.setColor(11 , "green")
			>>> graph.swap_color(2, 4)
			>>> graph.getColor(2) 
			blue
			>>> graph.getColor(8)
			black
		"""
		visit = {node1}
		file_fifo = [node1]
		while (len(file_fifo)!=0):
			x = file_fifo[0]
			file_fifo.remove(x)
			for y in self.getNeighbors(x): 
				if (self.getColor(y)==self.getColor(node1)  or self.getColor(y)==self.getColor(node2)) and self.getColor(y)!=self.getColor(x):
					if y not in visit:
						file_fifo.append(y)
						visit.add(y)
		new_color1 = self.getColor(node1)
		new_color2 = self.getColor(node2)
		for node in visit:
			if self.getColor(node) == new_color1:
				self.setColor(node, new_color2)
			else:
				self.setColor(node, new_color1)
	
	
	
	def coloring(self) -> None:
		"""
			Coloring a graph with the recursive method

			:return:	none.
			:rtype:		None
			:Exemple:
			>>> graph = graph.Graph(3)
			>>> graph.coloring()
			"""
		if(not self.is_empty()):
			x = self.getNodeDegreeInfFive()
			# ~ G − x
			self.deleteNode(x)
			self.coloring()
			# ~ Remettre le sommet x dans le graphe
			self.setActive(x,True)
			self.increaseSize()   
			# ~ Brique 4
			if self.getNumberOfNeighbors(int(x))<=4:
				color = self.getColorNotUsedByNeighbors(x)
				self.setColor(x, color)
				log.info(f"Brique 4: coloration du noeud {x} en {color}.")
			elif self.isLessFiveColor(x):  # ~ Brique 5
				color = self.getColorNotUsedByNeighbors(x)
				self.setColor(x, color)
				log.info(f"Brique 5: coloration du noeud {x} en {color}.")
			else : # Brique 6
				list_nodes = list()
				for neighbor in self.getNeighbors(x):
					list_nodes.append(neighbor)
				sommet_a = list_nodes[0]
				sommet_b = list_nodes[1]
				sommet_c = list_nodes[2]
				sommet_d = list_nodes[3]
				sommet_e = list_nodes[4]
				if not self.sameRelatedComponent(sommet_a,sommet_c):
					self.swap_color(sommet_a,sommet_c)
					log.info(f"Brique 6: échange des couleurs des noeuds {sommet_a} et {sommet_c}.")
					color = self.getColorNotUsedByNeighbors(x)
					self.setColor(x, color)
					log.info(f"Brique 6: coloration du noeud {x} en {color}.")
				elif not self.sameRelatedComponent(sommet_b,sommet_d):
					self.swap_color(sommet_b,sommet_d)
					log.info(f"Brique 6: échange des couleurs des noeuds {sommet_b} et {sommet_d}.")
					color = self.getColorNotUsedByNeighbors(x)
					self.setColor(x, color)
					log.info(f"Brique 6: coloration du noeud {x} en {color}.")
	
	
	
	def checkColoring(self)-> bool:
		"""
			Check the coloring of a grape
			
			:return:	True if the coloring is good
			:rtype:		bool
			:Exemple:
			>>> graph = graph.Graph(3)
			>>> graph.coloring()
			>>> graph.testing()
			
			:return:	True if the coloring is good.
			:rtype:		bool
		
			:Exemple:
			>>> graph = graph.Graph(3)
			>>> graph.coloring()
			>>> graph.checkColoring()
			True
		"""
		log.info("Démarage de la vérification de l'afectation des couleurs.")
		for node in range(self.size):
			for neighbors in self.getNeighbors(node):
				node_color = self.getColor(node)
				if node_color == self.getColor(neighbors) or node_color == self.COLOR_ANY:
					log.warning(f"Coloration invalide pour le sommet '{node}', avec une couleur '{node_color}'.")
					return False
		log.success("Coloration valide de tous les sommets du graphe.")
		return True				


