# ==================== INFORMATIONS
__version__ = "1.0.0"
__author__ = "Arnaud Orlay <arnaud.orlay@etu.univ-orleans.fr>"



# ==================== EXPORT
__all__ = ["readFileGraph", "readFileCoords", "writeFileColors"]



# ==================== IMPORT
from coloration.utils.graph import Graph
from coloration.utils import log
import re



# ==================== PUBLIC FUNCTION
def readFileGraph(filepath: str) -> Graph:
	"""
		Read graph file

		Inject data of graph file in data structure.

		:param filepath:	graph file source.
		:type filepath:		str
		:return:			data structure
		:rtype:				Graph
	"""
	file_lines, size = __readFile(filepath=filepath)
	graph = Graph(size)
		
	for line in file_lines:
		match = re.search(pattern="^(\d+):\s\[(.*)\]$", string=line)
		for node in match.group(2).split(", "):
			if (len(node) > 0):
				graph.addNeighbor(int(match.group(1)), int(node))
		
	log.success(f"Injection des données du fichier '{__fileName(filepath)}' dans le graphe.")

	return graph



def readFileCoords(filepath: str, graph: Graph) -> Graph:
	"""
		Read coordinate file

		Inject data of coordinate file in data structure.

		:param filepath:	coordinate file source.
		:param graph:		graph.
		:type filepath:		str
		:type graph:		Graph
		:return:			data structure
		:rtype:				Graph
	"""
	file_lines, size = __readFile(filepath=filepath)

	if (graph.size != size):
		log.error(f"Impossible d'importer {size} coordonnées dans un graphe à {graph.size} sommets.")
	else:
		for line in file_lines:
			match = re.search(pattern="^(\d+):\s*\((\d+),\s*(\d+)\)\n?$", string=line)
			graph.setCoordinate(int(match.group(1)), (int(match.group(2)), int(match.group(3))))
		
		graph.enableCoords()
		log.success(f"Injection des données du fichier '{__fileName(filepath)}' dans le graphe.")
		
	return graph



def writeFileColor(filepath: str, graph: Graph) -> None:
	"""
		Write color file

		Export data of color of graph in output file.

		:param filepath:	color file source.
		:param graph:		graph.
		:type filepath:		str
		:type graph:		Graph
		:return:			none.
		:rtype:				None
	"""
	with open(file=filepath, mode="w") as file:
		log.info(f"Création du fichier '{__fileName(filepath)}'.")
		file.write(f"{graph.size}\n")

		for node in range(graph.size):
			file.write(f"{node}: {graph.getColor(node)}\n")

	log.success(f"Export des données de coloration du graphe dans fichier '{__fileName(filepath)}'.")



# ==================== PRIVATE FUNCTION
def __readFile(filepath: str) -> (list, int):
	"""
		Read file

		:param filepath:	graph file source.
		:type filepath:		str
		:return:			number of nodes and each node with their neighbors.
		:rtype:				(list, int)
	"""
	file_lines = list()
	size = 0

	try:
		with open(file=filepath, mode="r") as file:
			log.info(f"Ouverture du fichier '{__fileName(filepath)}'.")
			file_lines = file.readlines()
			log.info(f"Récupération des données du fichier '{__fileName(filepath)}'.")
		size = int(file_lines.pop(0))
	except FileNotFoundError:
		log.error(f"Le fichier '{__fileName(filepath)}' n'existe pas.")
		
	return (file_lines, size)



def __fileName(filepath: str) -> str:
	"""
		Get file name from file path

		:param filepath:	file path.
		:type filepath:		str
		:return:			name of file.
		:rtype:				str
	"""
	return filepath.split("/")[-1]