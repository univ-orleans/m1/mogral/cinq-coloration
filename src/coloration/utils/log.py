# ==================== INFORMATIONS
__version__ = "1.0.0"
__author__ = "Arnaud Orlay <arnaud.orlay@etu.univ-orleans.fr>"



# ==================== EXPORT
__all__ = ["info", "success", "warning", "error"]



# ==================== IMPORT
from datetime import datetime



# ==================== PUBLIC FUNCTION
def info(message: str) -> None:
    """
        Print information log.

        :param message: message to print.
        :type:          str
        :return:        none.
        :rtype:         None

        :Exemple:
        >>> info("My message.")
        [2020-10-28] INFO   : My message.
    """
    __printLog("INFO   ", message)



def success(message: str) -> None:
    """
        Print information log.

        :param message: message to print.
        :type:          str
        :return:        none.
        :rtype:         None

        :Exemple:
        >>> success("My message.")
        [2020-10-28] SUCCESS: My message.
    """
    __printLog("SUCCESS", message)



def warning(message: str) -> None:
    """
        Print information log.

        :param message: message to print.
        :type:          str
        :return:        none.
        :rtype:         None

        :Exemple:
        >>> warning("My message.")
        [2020-10-28] WARNING: My message.
    """
    __printLog("WARNING", message)



def error(message: str) -> None:
    """
        Print information log.

        :param message: message to print.
        :type:          str
        :return:        none.
        :rtype:         None

        :Exemple:
        >>> warning("My message.")
        [2020-10-28] ERROR  : My message.
    """
    __printLog("ERROR  ", message)



# ==================== PRIVATE FUNCTION
def __getDate() -> str:
    """
        Get formated date.

        :return:    formated date and time.
        :rtype:     str

        :Exemple:
        >>> __getDate()
        2020-10-29 09:36:06

        .. note: private method.
    """
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")[:-2]



def __printLog(label: str, message: str) -> None:
    """
        Print message on system consol.

        :Exemple:
        >>> __printLog("INFO   ", Création du graphe à 10 noeuds.)
        [2020-10-29 09:36:06] INFO    Création du graphe à 10 noeuds.

        .. note: private method.
    """
    from coloration.__main__ import args
    if args.log:
        print(f"\033[90m[{__getDate()}] \033[33m{label}\033[0m {message}")