# ==================== INFORMATIONS
__version__ = "1.0.0"
__author__ = "Arnaud Orlay <arnaud.orlay@etu.univ-orleans.fr>"



# ==================== EXPORT
__all__ = ["saveSvgFile"]



# ==================== IMPORT
from coloration.utils.graph import Graph
from coloration.utils import log
import subprocess



# ==================== CONSTANTE
CIRCLE_RAY = 0.25 
LINE_STROKE_WIDTH = CIRCLE_RAY / 4
LINE_STROKE = "#000000"
GRID_STROKE_WIDTH = 0.025
GRID_STROKE = "#DFDFDF"
COLOR_ANY = "transparent"
BACKGROUND = "#FFFFFF"
INIT_SIZE = 2048



# ==================== PUBLIC FUNCTION
def saveSvgFile(filepath: str, graph: Graph) -> None:
    """
        Export graph in SVG file.

        :param filepath:    SVG file source.
        :param graph:       graph.
        :type filepath:     str
        :type graph:        Graph
        :return:            none.
        :rtype:             None
    """
    size_x, size_y = __svgSize(graph)
    res = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
    res += f"<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"{INIT_SIZE * size_x // size_y}px\" height=\"{INIT_SIZE}px\" viewBox=\"0 0 {size_x} {size_y}\" style=\"width:100%;height:100%\">\n\n"
    res += "\t<defs>\n"
    res += f"\t\t<pattern id=\"grid\" viewBox=\"0 0 1 1\" width=\"{100/size_x}%\" height=\"{100/size_y}%\">\n"
    res += f"\t\t\t<rect width=\"1\" height=\"1\" fill=\"{BACKGROUND}\" stroke=\"{GRID_STROKE}\" stroke-width=\"{GRID_STROKE_WIDTH}\"/>\n"
    res += "\t\t</pattern>\n"
    res += "\t</defs>\n\n"
    res += "\t<rect fill=\"url(#grid)\" x=\"0\" y=\"0\" width=\"100%\" height=\"100%\"/>\n\n"

    res += f"\t<g stroke=\"{LINE_STROKE}\" stroke-width=\"{LINE_STROKE_WIDTH}\">\n"
    pile = list()
    for node in range(graph.size):
        for neighbor in graph.getNeighbors(node):
            edge = {node, neighbor}
            if edge not in pile:
                pile.append(edge)
                res += __getLine(graph.getCoordinate(node), graph.getCoordinate(neighbor), size_y)
    
    for node in range(graph.size):
        color = graph.getColor(node)
        if color == graph.COLOR_ANY:
            color = COLOR_ANY
        res += __getCircle(graph.getCoordinate(node), color, size_y, node)
    
    res += "\t</g>\n</svg>"

    with open(file=filepath, mode="w") as file:
        log.info(f"Création du fichier SVG '{__fileName(filepath)}'.")
        file.write(res)
    
    log.success(f"Export de la représentation du graphe dans fichier SVG '{__fileName(filepath)}'.")

    proc = subprocess.run([f"exo-open --launch WebBrowser {filepath} &"], shell=True, stderr=subprocess.PIPE, stdout=subprocess.DEVNULL, check=True)
    if proc.returncode or len(proc.stderr):
        log.warning(f"Impossible d'ouvrir {__fileName(filepath)} dans le navigateur par défaut.")
    else:
        log.info(f"Affichage de {__fileName(filepath)} dans le navigateur par défaut.")



# ==================== PRIVATE FUNCTION
def __getLine(coords_node: (int, int), coords_neighbor: (int, int), size_y: int) -> str:
    """
        Get SVG tag line.

        :param coords_node:     node coordinate in plane of degree 2.
        :param coords_neighbor: neighbor coordinate in plane of degree 2.
        :type coords_node:      (int, int)
        :type coords_neighbor:  (int, int)
        :return:                svg tag.
        :rtype:                 str
    """
    return f"\t\t<line x1=\"{coords_node[0]}\" y1=\"{size_y - coords_node[1]}\" x2=\"{coords_neighbor[0]}\" y2=\"{size_y - coords_neighbor[1]}\"/>\n"



def __getCircle(coords_node: (int, int), color: str, size_y: int, node: str) -> str:
    """
        Get SVG tag circle.

        :param coords_node: node coordinate in plane of degree 2.
        :param color:       node color.
        :param node: 		node
        :type coords_node:  (int, int)
        :type color:        str
        :type node:        str
        :return:            svg tag.
        :rtype:             str
    """
    return f"\t\t<circle cx=\"{coords_node[0]}\" cy=\"{size_y - coords_node[1]}\" r=\"{CIRCLE_RAY}\" fill=\"{color}\"  data-node=\"{node}\"/>\n"



def __fileName(filepath: str) -> str:
	"""
		Get file name from file path

		:param filepath:	file path.
		:type filepath:		str
		:return:			name of file.
		:rtype:				str
	"""
	return filepath.split("/")[-1]



def __svgSize(graph: Graph) -> (int, int):
    """
        Get max value coordinate for x and y.

        :param graph:       graph.
        :type graph:        Graph
        :return:            highter for x and y.
        :rtype:             (int, int)
    """
    max_x = 0
    max_y = 0

    for node in range(graph.size):
        coords_x, coords_y = graph.getCoordinate(node)
        max_x = max(coords_x, max_x)
        max_y = max(coords_y, max_y)
    
    return (max_x+1, max_y+1)